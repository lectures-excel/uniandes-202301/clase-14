
rm(list=ls())

require(pacman)

p_load(tidyverse,rio)

 FINAL_DASHBOARD <- import("clase_14/data/FINAL DASHBOARD.xlsx",  sheet = "Hoja1")
 
anova<- aov(data = FINAL_DASHBOARD,formula = FINAL_DASHBOARD$doses_administered_by_100_people ~ FINAL_DASHBOARD$`Income Level`,qr = TRUE)

Plot<- ggplot(data=FINAL_DASHBOARD,aes(x=FINAL_DASHBOARD$`Income Level` ,y=FINAL_DASHBOARD$doses_administered_by_100_people))+geom_boxplot(aes(fill= FINAL_DASHBOARD$`Income Level`))+theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+ theme(axis.text.x=element_text(angle=50, size=10, vjust=0.5))

Plot <- Plot + labs(x="Income Group",y="Doses administered by every 100 people",title = "Vaccine distribution by income level",subtitle = "Data as of Nov-16-2021, Source: Our World in Data and the World Bank",fill="")

Plot

summary(anova)

ggsave(plot = Plot,width = 7,height = 6,filename = "Access to vaccines worldwide.jpg")
